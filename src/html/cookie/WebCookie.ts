import isString from 'lodash/isString'

const EQ_SYMBOL = '='
const SP_COOKIE = '; '
const RP_DECODE = /(%[0-9A-Z]{2})+/g

const DecCookie = (str: string): string => str.replace(RP_DECODE, decodeURIComponent)

class WebCookie {
  get(key?: string): string {
    const cookies = {}
    const content = document.cookie
    if (content && content.length > 0) {
      const section = content.split(SP_COOKIE)
      for (let l = section.length, i = 0, cut, val, tag; i < l; i++) {
        val = DecCookie((cut = section[i].split(EQ_SYMBOL)).slice(1).join(EQ_SYMBOL))
        try {
          cookies[tag = DecCookie(cut[0])] = val
          if (key === tag) {
            return val
          }
        // eslint-disable-next-line no-empty
        } catch (e) {}
      }
    }
    return isString(key) ? cookies[key!] : cookies
  }
}

export default new WebCookie()
