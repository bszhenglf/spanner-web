import I18n from '../i18n'

class Spanner {
  private _one
  private _provider
  uc
  i18n
  request

  constructor() {
    this._one = {}
  }

  /**
   * Spanner初始化
   */
  init(provider) {
    (this._provider = provider).init()
    // 获取one实例
    this._one = provider.getOne()
    // 获取uc实例
    this.uc = provider.getUC()
    // 挂载i18n实例
    this.i18n = new I18n(
      provider.initParams.i18n || {}
    )
    // 获取request实例
    this.request = provider.getRequest()
    // 分析请求故障原因
    this.request.analyzeFault = function (e, defaultVal = '') {
      let msg = defaultVal
      if (e) {
        const resp = e.response
        if ((resp || 0).data) {
          // 请求响应错误处理逻辑
          msg = resp.data.message
        } else if (e.message) {
          // 非请求响应错误处理逻辑
          msg = e.message
        }
      }
      return msg
    }
  }

  getEnv(): string {
    if (this._provider) {
      return this._provider.getEnv()
    }
    return ''
  }

  getLang(): string {
    if (this._provider) {
      const locale = this.i18n.getLocale()
      if (locale) {
        return locale.require || locale.default
      }
    }
    return ''
  }

  getSdpAppId(): string {
    if (this._provider) {
      return this._provider.getSdpAppId()
    }
    return ''
  }

  getSdpOrgId(): string {
    if (this._provider) {
      return this._provider.getSdpOrgId()
    }
    return ''
  }

  getSdpBizType(): string {
    if (this._provider) {
      return this._provider.getSdpBizType()
    }
    return ''
  }
}

export default new Spanner()
