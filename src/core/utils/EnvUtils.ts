import { ENV_LHIP, ENV_ABBR, ENV_FULL } from './const'

interface Out {
  abbr: string
  full: string
}

export const parse = (env): Out => {
  const out: Out = {
    abbr: env,
    full: env
  }
  const loc = ENV_LHIP[1]
  if (env && env.length) {
    if (env !== loc) {
      const idx = (ENV_FULL.indexOf(env) > -1 ? ENV_FULL.indexOf(env) : ENV_ABBR.indexOf(env))
      if (idx > -1) {
        out.abbr = ENV_ABBR[idx]
        out.full = ENV_FULL[idx]
      } else {
        out.abbr = loc
        out.full = loc
      }
    }
  } else {
    out.abbr = loc
    out.full = loc
  }
  return out
}

export const isLocalhost = (list: string[] = [], host = window?.location?.hostname): boolean => {
  return ENV_LHIP.indexOf(host) !== -1 || list.includes(host)
}
