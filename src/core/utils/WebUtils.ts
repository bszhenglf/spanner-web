import isArray from 'lodash/isArray'
import isObject from 'lodash/isObject'
import isNumber from 'lodash/isNumber'
import isEqual from 'lodash/isEqual'

import {
  ARR_INTRO, STR_INTRO, NUM_INTRO, OBJ_INTRO,
  BLN_INTRO, PMS_INTRO, FUN_INTRO, INT_INCL0,
  INT_EXCL0, DEC_REGEX, GAP_CLICK
} from './const'

interface FastClick {
  (): boolean
}

const _getProtoIntro = <T>(obj: T) => {
  return Object.prototype.toString.call(obj)
}

/**
 * 是否定义
 */
export const isDefined = (obj?: any): boolean => {
  if (isNumber(obj)) {
    return !isNaN(obj)
  }
  return !isEqual(obj, undefined)
}

/**
 * 是否为 Promise
 *
 * @param {对象} obj
 */
export const isPromise = (obj: any): boolean => {
  return _getProtoIntro(obj) === PMS_INTRO
}

/**
 * @description: 是否快速点击 闭包加立即执行
 */
export const isFastClick = (() => {
  let lastClick = new Date().valueOf()
  return function (): boolean {
    const newClick = new Date().valueOf()
    const diffTime = newClick - lastClick
    lastClick = newClick
    return diffTime < GAP_CLICK ? true : false
  }
})()

/**
 * @description: 是否快速点击 闭包加传参
 * @param {number} gap，与上次点击间隔的时间
 * @return {Function}
 */
export const getFastClickJudge = (gap: number): FastClick => {
  let lastClick = new Date().valueOf()
  return function (): boolean {
    const newClick = new Date().valueOf()
    const diffTime = newClick - lastClick
    lastClick = newClick
    return diffTime < gap ? true : false
  }
}
