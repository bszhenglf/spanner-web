export const FMT_REGEX = /\$([1-9]\d*)/g
export const TXT_SPACE = /(^[\s\xa0\u3000]+)|([\s\xa0\u3000]+$)/g
export const INT_INCL0 = /^[+-]?\d+(e+[1-9]\d*)?$/i
export const INT_EXCL0 = /^[+-]?[1-9]\d*(e+[1-9]\d*)?$/i
export const DEC_REGEX = /^[+-]?\d+(\.\d*(e-[1-9]\d*)?|e-[1-9]\d*)$/i

export const QUEST_IS = /\?/
export const SLASH_IS = /^\//
export const POUND_RP = /^#+/g
export const QUERY_RP = /@|'/g
export const SLASH_RP = /\/+$/g
export const VISIT_IS = /^https?:\/\//i
export const MATCH_KV = /([^?&=]+)(?:=([^?&=]*))/g
// 匹配/v<number>且不是//v<number>
// (避免匹配域名为 http://v<number>.xxx.xxx 的情况)
export const MATCH_VER = /[^/]\/v\d+/
// 匹配<prefix><query><hash>
export const SLICE_URL = /([^?#]*)(\?(?:[^#]*))?(#(?:.*))?/
// 匹配<protocol>//<hostAndPort><path><query><hash>
export const PARSE_URL = /^((?:[^:/?#]+):)?(?:\/\/([^/?#]*))?([^?#]*)(\?(?:[^#]*))?(#(?:.*))?/
// 匹配Anchor
export const PARSE_ANCHOR = /([^?]*)(\?.*)?/
// 匹配Host和Port
export const PARSE_SERVER = /([^:]+)(?::(\d+))?/


export const GAP_CLICK = 700
export const ARR_INTRO = '[object Array]'
export const STR_INTRO = '[object String]'
export const NUM_INTRO = '[object Number]'
export const OBJ_INTRO = '[object Object]'
export const BLN_INTRO = '[object Boolean]'
export const PMS_INTRO = '[object Promise]'
export const FUN_INTRO = '[object Function]'

export const ENV_LHIP = ['127.0.0.1', 'localhost', '::1']
export const ENV_ABBR = ['dev', 'pre', 'pro', 'awsca']
export const ENV_FULL = ['development', 'preproduction', 'product', 'aws-california']

export const GJCFG_ID = 'gaea_js_config'
export const TROPE_KV = {
  '@': '%40',
  '\'': '%27'
}