import isArray from 'lodash/isArray'
import isEmpty from 'lodash/isEmpty'
import isString from 'lodash/isString'
import { FMT_REGEX, TXT_SPACE } from './const'

/**
 * 是否空白
 * @param {文本} txt
 */
export const isBlank = (txt: string): boolean => {
  return isString(txt) && isEmpty(trim(txt))
}

/**
 * 去除文本头尾空格
 * @param {文本} txt
 */
export const trim = (txt: string): string => {
  return isString(txt) ? txt.replace(TXT_SPACE, '') : txt
}

/**
 * 文本格式化
 * @param {文本} txt
 * @param {数组} arr
 */
export const format = <T>(txt: string, arr: Array<T>): string => {
  if (!isArray(arr) || !isString(txt)) {
    return txt
  }
  return txt.replace(FMT_REGEX, function (fmt: string, num: number): string {
    // 这里应该有推断的
    const _num = arr[num - 1] // 赋值后可以推断
    if (typeof _num === 'string') return _num
    return ''
  })
}
