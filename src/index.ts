import spanner from './core'
import EventBus, {
  WebEvent
} from './event'

import WebCookie from './html/cookie/WebCookie'
import WebMessage from './html/iframe/WebMessage'
import QueueNotice from './html/notice/QueueNotice'

import * as TxtUtils from './core/utils/TxtUtils'
import * as UrlUtils from './core/utils/UrlUtils'
import * as WebUtils from './core/utils/WebUtils'
import * as AppUtils from './core/utils/AppUtils'
import * as EnvUtils from './core/utils/EnvUtils'

export {
  spanner,
  AppUtils,
  EnvUtils,
  TxtUtils,
  UrlUtils,
  WebUtils,
  WebEvent,
  EventBus,
  WebCookie,
  WebMessage,
  QueueNotice
}

export default spanner
