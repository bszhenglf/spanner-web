function (data) {
  var blockOne = [];
  var blockTow = '';
  var teacherList = data.extra.teachers && data.extra.teachers.length ? [data.extra.teachers[0]] : [];
  for (var i = 0; i < teacherList.length; i++) {
    blockOne.push('<span>' + teacherList[i].display_name + '</span>');
  }
  if (data.extra.display_amount) {
    blockTow = '<div class="cell-info-price ' + (!data.extra.amount ? 'free' : '') + '">' + (data.extra.display_amount || i18nHelper.getKeyValue('channel.common.free')) + '</div>';
  }
  return data.card_type == 3 ? ['<div class="cell-info">', '<div class="cell-info-title" data-bind="text: title"></div>', '<div class="cell-info-other">', '<span>' + i18nHelper.getKeyValue('channel.cell.startTime') + '</span>', '<span class="strong">' + (data.start_time ? moment(data.start_time).format('MM-DD HH:mm') : '') + '</span>', '</div>', '<div class="cell-info-foot">', '<div class="cell-info-teacher">', blockOne.join(''), '</div>', blockTow, '</div>', '</div>'].join('') : '';
}