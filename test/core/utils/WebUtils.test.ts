/**
 * HelloWorld.test.ts
 */
import * as WebUtils from '../../../src/core/utils/WebUtils';

test('isDefined test', () => {
  expect(WebUtils.isDefined()).toBeFalsy()
  expect(WebUtils.isDefined(undefined)).toBeFalsy()
  expect(WebUtils.isDefined(null)).toBeTruthy()
  expect(WebUtils.isDefined(0)).toBeTruthy()
  expect(WebUtils.isDefined(1)).toBeTruthy()
  expect(WebUtils.isDefined('1')).toBeTruthy()
  expect(WebUtils.isDefined(true)).toBeTruthy()
})

test('isDefined test', () => {
  expect(WebUtils.isPromise({})).toBeFalsy()
  expect(WebUtils.isPromise(new Promise((resolve, reject) => {}))).toBeTruthy()
})

test('isFastClick test', () => {
  expect(WebUtils.isFastClick()).toBeTruthy()
  setTimeout(() => {
    expect(WebUtils.isFastClick()).toBeTruthy()
    setTimeout(() => {
      expect(WebUtils.isFastClick()).toBeFalsy()
      setTimeout(() => {
        expect(WebUtils.isFastClick()).toBeFalsy()
        setTimeout(() => {
          expect(WebUtils.isFastClick()).toBeTruthy()
        }, 701)
      }, 700)
    }, 699)
  }, 100)
})

test('getFastClickJudge test', () => {
  expect(WebUtils.getFastClickJudge(100)()).toBeTruthy()
  setTimeout(() => {
    expect(WebUtils.getFastClickJudge(100)()).toBeFalsy()
  }, 150)
  setTimeout(() => {
    expect(WebUtils.getFastClickJudge(150)()).toBeTruthy()
  }, 100)
})