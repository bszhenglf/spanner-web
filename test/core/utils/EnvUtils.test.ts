/**
 * HelloWorld.test.ts
 */
import * as EnvUtils from '../../../src/core/utils/EnvUtils';

test('parse test localhost', () => {
  expect(EnvUtils.parse('localhost')).toEqual({ abbr: 'localhost', full: 'localhost'})
})

test('parse test empty', () => {
  expect(EnvUtils.parse('')).toEqual({ abbr: 'localhost', full: 'localhost'})
})

test('parse test other', () => {
  expect(EnvUtils.parse('other')).toEqual({ abbr: 'localhost', full: 'localhost'})
})

test('parse test dev', () => {
  expect(EnvUtils.parse('dev')).toEqual({ abbr: 'dev', full: 'development'})
  expect(EnvUtils.parse('development')).toEqual({ abbr: 'dev', full: 'development'})
})

test('parse test pre', () => {
  expect(EnvUtils.parse('pre')).toEqual({ abbr: 'pre', full: 'preproduction'})
  expect(EnvUtils.parse('preproduction')).toEqual({ abbr: 'pre', full: 'preproduction'})
})

test('parse test pro', () => {
  expect(EnvUtils.parse('pro')).toEqual({ abbr: 'pro', full: 'product'})
  expect(EnvUtils.parse('product')).toEqual({ abbr: 'pro', full: 'product'})
})

test('parse test awsca', () => {
  expect(EnvUtils.parse('awsca')).toEqual({ abbr: 'awsca', full: 'aws-california'})
  expect(EnvUtils.parse('aws-california')).toEqual({ abbr: 'awsca', full: 'aws-california'})
})

test('isLocalhost test true', () => {
   // host 默认值是 localhost
  expect(EnvUtils.isLocalhost()).toBeTruthy()
  expect(EnvUtils.isLocalhost([])).toBeTruthy()
  expect(EnvUtils.isLocalhost([''])).toBeTruthy()
  expect(EnvUtils.isLocalhost(['www.abc.com', 'www.ebay.com'])).toBeTruthy()

  expect(EnvUtils.isLocalhost([], 'localhost')).toBeTruthy()
  expect(EnvUtils.isLocalhost([], '127.0.0.1')).toBeTruthy()
  expect(EnvUtils.isLocalhost([], '::1')).toBeTruthy()

  expect(EnvUtils.isLocalhost(['www.baidu.com'], 'localhost')).toBeTruthy()
  expect(EnvUtils.isLocalhost(['www.baidu.com'], '127.0.0.1')).toBeTruthy()
  expect(EnvUtils.isLocalhost(['www.baidu.com'], '::1')).toBeTruthy()
  expect(EnvUtils.isLocalhost(['www.baidu.com', 'www.ebay.com'], 'www.baidu.com')).toBeTruthy()
})

test('isLocalhost test false', () => {
  expect(EnvUtils.isLocalhost(['www.abc.com', 'www.ebay.com'], 'www.baidu.com')).toBeFalsy()
  expect(EnvUtils.isLocalhost(['www.abc.com', 'www.ebay.com'], '')).toBeFalsy()
  expect(EnvUtils.isLocalhost([], 'www.baidu.com')).toBeFalsy()
})
