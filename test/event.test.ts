/**
 * HelloWorld.test.ts
 */
import Event from '../src/event/index';

function genEvent(name, cb, priority = 0) {
  return {
    [name]: [
      {
        priority: priority,
        eventCb: cb
      }
    ]
  }
}

test('Event test', () => {
  const name0 = '0'
  const cb0 = () => {}
  const priority0 = -1
  Event.bind(name0, cb0, priority0)
  const event0 = genEvent(name0, cb0, 0)
  expect(Event.events).toStrictEqual(event0)

  const name1 = 'event1'
  const cb1 = () => { console.log('cb1') }
  const priority1 = 1
  Event.bind(name1, cb1, priority1)
  const event1 = genEvent(name1, cb1, priority1)
  expect(Event.events).toStrictEqual({...event1, ...event0})

  const cb2 = () => { console.log('cb2') }
  Event.bind(name1, cb2)
  expect(Event.events).toStrictEqual({
    'event1': [
      {
        priority: 1,
        eventCb: cb1
      },
      {
        priority: 0,
        eventCb: cb2
      }
    ],
    ...event0})

  Event.post(name0)
  Event.post(name1)
  Event.unbind(name1)
  expect(Event.events).toStrictEqual(event0)
  // expect().toStrictEqual({...event1, ...event0})
})
