import * as helper from '../../src/i18n/helper';

test('formatLocale test', () => {
  expect(helper.formatLocale('en-us')).toEqual('en-US')
  expect(helper.formatLocale('en-uS')).toEqual('en-US')
  expect(helper.formatLocale('en-uS-666adsf')).toEqual('en-US')
  expect(helper.formatLocale('en-u.S-666adsf')).toEqual('en-U.S')
  expect(helper.formatLocale('en-u.S-666s-sss')).toEqual('en-U.S')
})

test('compatLocale test', () => {
  expect(helper.compatLocale('en-us')).toEqual('en')
  expect(helper.compatLocale('en-us-sssss')).toEqual('en')
  expect(helper.compatLocale('En-us-sssss')).toEqual('En')
})