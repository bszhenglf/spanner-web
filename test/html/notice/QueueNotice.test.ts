import QueueNotice from '../../../src/html/notice/QueueNotice'

test('queue notice', () => {
  for (const t of [
    'info',
    'error',
    'success',
    'warning'
  ]) {
    const d = {
      title: t,
      content: t
    }
    const listener = jest.fn().mockImplementation((type, data, cb) => {
      expect(type).toBe(t)
      expect(data).toBe(d)
      cb.next()
    })
    QueueNotice.init(listener)
    QueueNotice[t](d)
    QueueNotice[t](d)
    expect(listener).toBeCalledTimes(2)
  }
})
