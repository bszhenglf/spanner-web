import cookie from '../../../src/html/cookie/WebCookie'

test('cookie', () => {
  Object.defineProperty(document, 'cookie', {
    get: jest
      .fn()
      .mockImplementation(
        () => 'cur_domain=d.101.com; CNZZDATA1256793290=1804824477-1600839120-https%253A%252F%252Fd.101.com%252F%7C1608607792'
      ),
    set: jest.fn().mockImplementation(() => {}),
  })
  expect(cookie.get('cur_domain')).toBe('d.101.com')
  expect(cookie.get('CNZZDATA1256793290')).toBe(
    '1804824477-1600839120-https%3A%2F%2Fd.101.com%2F|1608607792'
  )
  expect(cookie.get()).toMatchObject({
    cur_domain: 'd.101.com',
    CNZZDATA1256793290:
      '1804824477-1600839120-https%3A%2F%2Fd.101.com%2F|1608607792',
  })
})
